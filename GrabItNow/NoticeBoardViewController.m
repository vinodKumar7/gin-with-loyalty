//
//  NoticeBoardViewController.m
//  GrabItNow
//
//  Created by MyRewards on 3/25/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "NoticeBoardViewController.h"
#import "AppDelegate.h"
#import "ProductCell.h"
#import <QuartzCore/QuartzCore.h>

#define ProductCellHeight 84.0;

@interface NoticeBoardViewController ()
{
    AppDelegate *appDelegate;
    ASIFormDataRequest *noticeBoardRequest;
    NoticeBoardDataParser *NoticeBoardXMLParser;
    
    NSIndexPath * openProductIndex;
    int select;
    BOOL isRead;
}
@property (nonatomic, strong) NSIndexPath * openProductIndex;
-(void)fetchNotices;
@end

@implementation NoticeBoardViewController
@synthesize noticeWebView;
@synthesize notice;
@synthesize noticesTable;
@synthesize noticesListArray;
@synthesize openProductIndex;
@synthesize activityView;
@synthesize noticeContentView;
@synthesize noticeContentTextView;
@synthesize readMsgs_Array;
@synthesize readMsgInd_Array;
//============================ DELEGATE METHODS  ============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        openProductIndex = nil;
        
        // self.readMsgs_Array = [[NSMutableArray alloc]init];
        self.readMsgInd_Array = [[NSMutableArray alloc]init];
        isRead = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
    
    [appDelegate.homeViewController hideBackButon:YES];
    //self.noticesListArray = [[NSMutableArray alloc]init];
    
    self.noticesTable.backgroundColor = [UIColor clearColor];
    
    

    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    select=0;
    
    [self showActivityView];
    [self fetchNotices];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [appDelegate.homeViewController setHeaderTitle:@"Notice Board"];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}


//=======================================================//


//============================ SHOW ALERTVIEW METHODS  =============================//
- (void) showActivityView
{
    // activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}
- (void) dismissActivityView
{
    [activityView removeFromSuperview];
}
//==================================================================================//

//================================TO FETCH NOTICES ==============================================//
-(void)fetchNotices
{
    
    NSString *urlString = [NSString stringWithFormat:@"http://myrewards.com.au/newapp/get_notice.php?cid=1"];//%@",appDelegate.sessionUser.client_id];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is:==> %@",url);
    
    
    noticeBoardRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [noticeBoardRequest setDelegate:self];
    [noticeBoardRequest startAsynchronous];
    
    
    
}
//==================================================================================//


//============================ ASIHTTP REQUEST DELEGATE METHODS ====================//
#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    // NSLog(@"NoticeBoardVC -- requestFinished:");
    
    if (request == noticeBoardRequest)
    {
        NSString *result=[NSString stringWithFormat:@"%@", [request responseString]];
        // NSLog(@"**getImageNPidRequest  RES: %@",[request responseString]);
        NSXMLParser *noticeParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        
        if (result.length==0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Notices available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
            [alert show];
        }
        else{
            NoticeBoardXMLParser = [[NoticeBoardDataParser alloc] init];
            NoticeBoardXMLParser.delegate = self;
            noticeParser.delegate = NoticeBoardXMLParser;
            [noticeParser parse];
            
        }
        
        
        
    }
    [self dismissActivityView];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"NoticeBoardVC -- requestFailed:");
    
    if (request == noticeBoardRequest)
    {
        noticeBoardRequest = nil;
        [self dismissActivityView];
        
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];
        
    }
}
//==================================================================================//
//============================ TABLE VIEW DELEGATE METHODS ====================//

#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  [self.noticesListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //NSLog(@"tableView:heightForRowAtIndexPath:");
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        // NSLog(@"ROW=%d toBeHeight=%f",indexPath.row,self.noticesTable.frame.size.height);
        return self.noticesTable.frame.size.height;
    }
    
    
    if (indexPath.row == self.noticesListArray.count)
    {
        return 44.0;
    }
    
    return ProductCellHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"cellForRowAtIndexPath");
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == self.noticesListArray.count)
    {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell)
        {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        loadingCell.textLabel.text = @"Loading ....";
        loadingCell.textLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:255/255.0 green:97/255.0 blue:55/255.0 alpha:1.0];
        
        loadingCell.backgroundColor = [UIColor whiteColor];
        
        UIActivityIndicatorView *loadingActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loadingActivityView.backgroundColor = [UIColor clearColor];
        [loadingActivityView startAnimating];
        loadingCell.accessoryView = loadingActivityView;
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views)
        {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    float cellHeight = [self tableView:noticesTable heightForRowAtIndexPath:indexPath];
    float grayLineHeight = 1.0;
    float grayLineX = 0.0;
    float grayLineY = cellHeight - grayLineHeight;
    float grayLineWidth = tableView.frame.size.width;// - whiteLineX - (0.20 * tableView.frame.size.width);
    CGRect grayLineFrame = CGRectMake(grayLineX, grayLineY, grayLineWidth, grayLineHeight);
    UIView *grayLine = [[UIView alloc] initWithFrame:grayLineFrame];
    grayLine.alpha = 0.6;
    grayLine.backgroundColor = [UIColor lightGrayColor];
    
    // Do not pput a line in the last cell of section
    if (indexPath.row != ([self tableView:noticesTable numberOfRowsInSection:indexPath.section] - 1)) {
        [cell.contentView addSubview:grayLine];
    }
    
    NoticeBoard *currentnotice = [self.noticesListArray objectAtIndex:indexPath.row];
    cell.accesoryImageView.hidden = YES;
    cell.productImg.hidden = YES;
    cell.accesoryImageView.image = [UIImage imageNamed:@"arrow.png"];
    
    
    if(select == 0)
    {
        
        isRead = [self noticeObjectExistsInDb:currentnotice];
        
        if(isRead)
        {
            //NSLog(@"mesg read");
            cell.msgImage.image = [UIImage imageNamed:@""];
            [self.readMsgInd_Array replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        
        else
            cell.msgImage.image = [UIImage imageNamed:@"unread.png"];
        
        
    }
    
    
    
    if(select == 1)
    {
        //NSLog(@"Configuring cells after DidSelect row...");
        if([[self.readMsgInd_Array objectAtIndex:indexPath.row]  isEqualToString:@"1"])
            
            cell.msgImage.image = [UIImage imageNamed:@""];
        
        else
            cell.msgImage.image = [UIImage imageNamed:@"unread.png"];
    }
    
    // Set title and offer text here
    cell.noImageLabel.text = currentnotice.subject;
    cell.noticeLabel.hidden = NO;
    cell.noticeLabel.text = currentnotice.subject;
    cell.numberlabel.text = currentnotice.created_Date;
    
//    CGRect noticelabelframe= cell.noImageLabel.frame;
//    noticelabelframe.origin.x=60;
//    cell.noImageLabel.frame=noticelabelframe;
//    CGRect noticelabelframez= cell.noticeLabel.frame;
//    noticelabelframez.origin.x=60;
//    cell.noticeLabel.frame=noticelabelframez;
//    //NSLog(@"cell.noImageLabel.text::%@",cell.noImageLabel.text);
//    CGRect noticelabelframe1= cell.numberlabel.frame;
//    //noticelabelframe1.origin.x=60;
//    //noticelabelframe1.origin.y=30;
//    cell.numberlabel.frame=noticelabelframe1;
    
       
    // Set different background colors for the cells.
//    int rowModuleNo = indexPath.row % 2;
//    
//    if (rowModuleNo == 0) {
//        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
//    }
//    else if (rowModuleNo == 1) {
//        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
//    }
    
    
    
    
    // Show Product controller if indexpath == openProductIndex
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        //NSLog(@"Show Product controller if indexpath == openProductIndex");
        
        cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
//        cell.accesoryImageView.image = [UIImage imageNamed:@"UpAccessory.png"];
        
        cell.headerContainerView.layer.shadowRadius = 5.0;
        CGRect frame1 =cell.headerContainerView.frame;
        
        // NSLog(@"headercontainer height is ... %f",frame1.size.height);
        cell.headerContainerView.frame=frame1;
        
        
        
        
        CGRect frame = noticeContentView.frame;
        float headerHeight = ProductCellHeight;
        
        frame.origin.y = headerHeight;
        frame.origin.x = 0;
        frame.size.width = 320;
        // NSLog(@"noticeContentView :%f",(self.noticesTable.frame.size.height - headerHeight));
        frame.size.height = self.noticesTable.frame.size.height - headerHeight;
        noticeContentView.frame = frame;
        // [noticeIDArray count]
        
        NoticeBoard *currrentNotice = [self.noticesListArray objectAtIndex:indexPath.row];
        //self.noticeContentTextView.text = [currrentNotice.details stripHtml];
        
        
        //NSLog(@" html data is...........%@",myDescriptionHTML);
        
        [noticeWebView loadHTMLString:currrentNotice.details baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        /* CGRect noticewebframe=self.noticeWebView.frame;
         noticewebframe.size.width=320;
         self.noticeWebView.frame=noticelabelframe;*/
        
        // NSLog(@"** Calculated view frame: %@",NSStringFromCGRect(frame));
        
        
        [cell.contentView addSubview:self.noticeContentView];
        [cell.contentView bringSubviewToFront:cell.headerContainerView];
        
        
        
    }
    
    
    
    
    //========
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //NSLog(@"didSelectRowAtIndexPath");
    NoticeBoard *currentnotice = [self.noticesListArray objectAtIndex:indexPath.row];
    //NSLog(@" current index:: %d",currentnotice.index);
    
    if (indexPath.row == [self.noticesListArray count])
    {
        // Return since this is Loading more cell.
        return;
    }
    
    
    // **** Close the Product Details **** //
    if (openProductIndex)
    {
        
        openProductIndex = nil;
        
        
        int totalRows = [self.noticesTable numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.noticesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.noticesTable reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        
        
        self.noticesTable.scrollEnabled = YES;
        return;
    }
    
    
    
    
    // **** Open the Product Details **** //
    openProductIndex = indexPath;
    
    float toScrollOffsetY = openProductIndex.row * ProductCellHeight;
    
    // update content size
    if ( (self.noticesTable.contentSize.height - toScrollOffsetY) < self.noticesTable.frame.size.height)
    {
        self.noticesTable.contentSize = CGSizeMake(self.noticesTable.contentSize.width, (self.noticesTable.frame.size.height + toScrollOffsetY));
    }
    
    //Adding  NB id to coredata
    // NSLog(@"notice board Vc Prashanth1010");
    ProductCell *cell = (ProductCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.msgImage.image = [UIImage imageNamed:@"read.png"];
    
    
    select = 1;
    if([appDelegate nbExistsInCoredata:[self.noticesListArray objectAtIndex:indexPath.row]])
    {
        // NSLog(@"NB already available in Coredata notice board VC ");
    }
    
    else
    {
        //NSLog(@"NB Not available in Coredata notice board VC");
        [appDelegate addNoticeBoardID:[self.noticesListArray objectAtIndex:indexPath.row]];
        
        //[self.readMsgs_Array replaceObjectAtIndex:indexPath.row withObject:[self.noticesListArray objectAtIndex:indexPath.row]];
        appDelegate.badgecount = [self.noticesListArray count]-[[appDelegate noticeBoardIds] count];
        [self.readMsgInd_Array replaceObjectAtIndex:indexPath.row withObject:@"1"];
        
    }
    
    
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.noticesTable setContentOffset:CGPointMake(self.noticesTable.contentOffset.x, toScrollOffsetY)];
    } completion:^(BOOL finished) {
        
        int totalRows = [self.noticesTable numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.noticesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.noticesTable reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationBottom];
        }
        
    }];
    
    self.noticesTable.scrollEnabled = NO;
    
    
    
}

- (BOOL)noticeObjectExistsInDb:(NoticeBoard *)currNotice {
    
    for (int j=0; j<[self.readMsgs_Array count]; j++) {
        
        NoticeBoard *dBnb = [self.readMsgs_Array objectAtIndex:j];
        
        if([currNotice.notice_Id isEqualToString:dBnb.notice_Id])
            return YES;
    }
    
    return  NO;
    
}




//============================  Product Data Parser Delegate methodS  ==========================//

#pragma mark -- Product Data Parser Delegate methods

- (void) parsingNoticeBoardDataFinished:(NSArray *) noticesList
{
    if (!self.noticesListArray)
        self.noticesListArray = [[NSMutableArray alloc] initWithArray:noticesList];
    
    else{
        [self.noticesListArray removeAllObjects];
        [self.noticesListArray addObjectsFromArray:noticesList];
    }
    
    [self.readMsgInd_Array removeAllObjects];
    [self.readMsgs_Array removeAllObjects];
    
    for (int i = 0; i < [self.noticesListArray count]; i++)
        [self.readMsgInd_Array addObject:@"0"];
    
    
    self.readMsgs_Array = [[NSMutableArray alloc]initWithArray:[appDelegate noticeBoardIds]];
    
    if([self.noticesListArray count] > 0)
    {
        for (int i=0;i< [self.noticesListArray count];i++)
        {
            
            NoticeBoard *nb = [self.noticesListArray objectAtIndex:i];
            // NSLog(@"Webservice notice Id ::%@",nb.notice_Id);
            // NSLog(@"Webservice notice Index: %d",nb.index);
            
            for (int j=0; j<[self.readMsgs_Array count]; j++)
            {
                
                NoticeBoard *dBnb = [self.readMsgs_Array objectAtIndex:j];
                
                if([nb.notice_Id isEqualToString:dBnb.notice_Id])
                    [self.readMsgInd_Array replaceObjectAtIndex:nb.index withObject:@"1"];
            }
            
            
        }
    }
    
    
    
    
    
    
    [self.noticesTable reloadData];
    [self dismissActivityView];
    
    
}



- (void) parsingNoticeBoardDataXMLFailed
{
    
}
//==================================================================================//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    noticeWebView = nil;
    [super viewDidUnload];
}

@end
