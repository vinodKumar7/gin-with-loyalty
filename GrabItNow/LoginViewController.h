//
//  LoginViewController.h
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "UserDataXMLParser.h"
#import "CustomIOS7AlertView.h"

@class CAEmitterLayer;
@interface LoginViewController : UIViewController <UITextFieldDelegate, ASIHTTPRequestDelegate, NSXMLParserDelegate, UserDataXMLParser,UIActionSheetDelegate, UIPickerViewDelegate,UIPickerViewDataSource, CustomIOS7AlertViewDelegate>
{
    BOOL processRenwalOfToken;
}
@property (strong)						CAEmitterLayer	*myEmitter;
@property (strong, nonatomic) IBOutlet UIImageView *login_bgImageView;

@property (strong, nonatomic) IBOutlet UIPageControl *coarouselPage_Control;
@property (strong, nonatomic) IBOutlet UIImageView *screens_ImageView;


//@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (nonatomic, readwrite) BOOL processRenwalOfToken;

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *subDomainTextField;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIView *loginContainerView;
@property (strong, nonatomic) IBOutlet UIView *loginContainer_Container;


// firsttime login
@property (strong, nonatomic) IBOutlet UIView *firsttimelogin_ContainerView;
@property (strong, nonatomic) IBOutlet UIButton *firsttimeLoginButton;
@property (strong, nonatomic) IBOutlet UITextField *firstname_TextField;
@property (strong, nonatomic) IBOutlet UITextField *lastname_TextField;
@property (strong, nonatomic) IBOutlet UITextField *password_TextField;
@property (strong, nonatomic) IBOutlet UITextField *conformpwd_TextField;
@property (strong, nonatomic) IBOutlet UITextField *email_TextField;
@property (strong, nonatomic) IBOutlet UITextField *state_TextField;
@property (strong, nonatomic) IBOutlet UITextField *referralno_TextField;
@property (strong, nonatomic) IBOutlet UITextView  *tc_TextView;
@property (strong, nonatomic)          UITextField *membership_TextField;
@property (strong, nonatomic)          UITextField *alertTextField;
@property (strong, nonatomic)          UITextField *subdomain7TextField;
@property (strong, nonatomic)          UITextField *webAddress_TextField;
@property (strong, nonatomic) IBOutlet UIButton    *firsttimeLogin_submitButton;
@property (strong, nonatomic) IBOutlet UIButton *checkButton;
@property (strong, nonatomic) IBOutlet UIButton *newsLetter_Button;
@property (strong, nonatomic) IBOutlet UIView *processing_View;


- (IBAction)firsttimeLoginDetailsSubmit_ButtonTapped:(id)sender;
- (IBAction)tandcButton_Tapped:(id)sender;
- (IBAction)closeButton_Tapped:(id)sender;

- (UIViewController *) controllerAtIndex:(NSInteger) index ;

@end
