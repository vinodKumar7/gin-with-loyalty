//
//  FreebiesViewController.m
//  CoffeeApp
//
//  Created by vairat on 23/04/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "FreebiesViewController.h"
#import "LoyalityTableViewCell.h"
#import "ASIFormDataRequest.h"
#import "ProductListParser.h"
#import "Product.h"
#import "KeyboardViewController.h"

@interface FreebiesViewController ()
{
    ASIFormDataRequest *freebiesListRequest;
    ProductListParser  *productsXMLParser;
    UIImage *fetchedProductImage;

}
@property(nonatomic, strong)UIImage *fetchedProductImage;
@end

@implementation FreebiesViewController
@synthesize freebiesList,freebiesTableView,fetchedProductImage;

- (void)viewDidLoad {
    [super viewDidLoad];
     self.title = @"My Freebies";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{

       NSString *urlString = [NSString stringWithFormat:@"http://184.107.152.53/newapp/freebies_list.php"];
     NSURL *url = [NSURL URLWithString:urlString];
     // NSLog(@"url is %@
     //  NSLog(@"url is %@ product_ID is %@ device ID id %@",url,product_ID,deviceID);
     freebiesListRequest = [[ASIFormDataRequest alloc] initWithURL:url];
     [freebiesListRequest setPostValue:@"2A240C91-C641-45CD-A8B7-6F96B3498A9D" forKey:@"uid"];
     // [freebiesRequest setPostValue:product_id forKey:@"pid"];
     [freebiesListRequest setPostValue:@"24" forKey:@"cid"];
     //  [freebiesRequest setPostValue:@"1"forKey:@"quantity"];
     // [freebiesRequest setPostValue:self.collectionViewManager.OProduct.productDesciption forKey:@"pdesc"];
     // [freebiesRequest setPostValue:@"update" forKey:@"function"];
     [freebiesListRequest setDelegate:self];
     [freebiesListRequest startAsynchronous];


}
-(BOOL)prefersStatusBarHidden{

    return YES;
}
#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [freebiesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *cellIdentifier = @"loyaltyDetailCell";
    
    LoyalityTableViewCell *cell;
    
    if (cell == nil)
        cell = (LoyalityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
   // http://184.107.152.53/files/product_image/1029846.jpg
    cell.tag = indexPath.row;
    Product *pro = [freebiesList objectAtIndex:indexPath.row];
    NSLog(@"%@ pro.productName %@ %@ %@ %@ %@ pro.carouselImgLink is %@  pro.productIdis %@ ",pro,pro.productName,pro.productOffer,pro.termsAndConditions,pro.redeemPassword,pro.quantity,pro.carouselImgLink,pro.productId);
    cell.lblTitle.text =pro.productName;
    cell.lblSubTitle.text =pro.productOffer;
//    cell.quantity.text=[NSString stringWithFormat:@"Quantity :%@",pro.quantity];
    
    
    
  /*  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
    
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.product_ImageView.image = image;
                   // [productImagesArray addObject:image];
                    [cell setNeedsLayout];
                    
                    //[activityIndicatorBaseView removeFromSuperview];
                }
            });
        }
    });
    */
    cell.product_ImageView.image = nil;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        //NSLog(@"");
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.product_ImageView.image = image;
                    //[productImagesArray addObject:image];
                    // NSLog(@"count of product image is %d ",productImagesArray.count);
                    [cell setNeedsLayout];
                }
            });
        }
    });

    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UIStoryboard *storyboard = self.storyboard;
//    KeyboardViewController *svc = [storyboard instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
//    //[[UIStoryboard storyboardWithName:@"StoryboardNameOfnewSurveyView" bundle:nil] instantiateViewControllerWithIdentifier:@"newSurveyView"];
//    
//  ///  svc.OProduct= [freebiesList objectAtIndex:indexPath.row];
//    // Configure the new view controller here.
//    [self presentViewController:svc animated:NO completion:nil];
    
   // self.fetchedProductImage = cell.product_ImageView.image;
    
    indexPath = [self.freebiesTableView indexPathForSelectedRow];
    
    LoyalityTableViewCell *cell = (LoyalityTableViewCell *)[freebiesTableView cellForRowAtIndexPath:indexPath];
    self.fetchedProductImage = cell.product_ImageView.image;
    Product *pro = [freebiesList objectAtIndex:indexPath.row];
    KeyboardViewController *kvc = [[KeyboardViewController alloc]init];
    kvc.product_ID    = pro.productId;
    kvc.product_Image = self.fetchedProductImage;
    kvc.product_Name  = pro.productName;
    kvc.deviceID = @"2A240C91-C641-45CD-A8B7-6F96B3498A9D";
    kvc.isFromFreebies=YES;
    //        kvc.isLastStamp = isLastStamp;
    kvc.redeemPassword = pro.redeemPassword;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- RequestFinished and RequestFailed

- (void)requestFinished:(ASIHTTPRequest *)request
{
     NSLog(@"=======>> Product:: %@",[request responseString]);
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"=======>> Product:: %@",[request responseString]);
    
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    
}
#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
   // NSLog(@"product name %@",prodcutsListLocal);
     Product *pro = [prodcutsListLocal objectAtIndex:0];
    NSLog(@"product nam %@ product offer %@ ",pro.productName,pro.productOffer);
    if (!freebiesList)
        
        freebiesList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
        
        [freebiesList addObjectsFromArray:prodcutsListLocal];
    
    
    [freebiesTableView reloadData];
    
    
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    
    if ([segue.identifier isEqualToString:@"KeyBoardVC"])
    {
        NSIndexPath *indexPath = [self.freebiesTableView indexPathForSelectedRow];
        
        LoyalityTableViewCell *cell = (LoyalityTableViewCell *)[freebiesTableView cellForRowAtIndexPath:indexPath];
        self.fetchedProductImage = cell.product_ImageView.image;
       Product *pro = [freebiesList objectAtIndex:indexPath.row];
        KeyboardViewController *kvc = segue.destinationViewController;
        kvc.product_ID    = pro.productId;
       kvc.product_Image = self.fetchedProductImage;
        kvc.product_Name  = pro.productName;
        kvc.deviceID = @"2A240C91-C641-45CD-A8B7-6F96B3498A9D";
        kvc.isFromFreebies=YES;
//        kvc.isLastStamp = isLastStamp;
        kvc.redeemPassword = pro.redeemPassword;
//        kvc.multipleRedeemCount = self.collectionViewManager.selectedStampCount;
//        NSLog(@"self.collectionViewManager.selectedStampCount:: %@",self.collectionViewManager.selectedStampCount);
//        controlCameFromKeyboardView = YES;
    }


}
- (void)parsingProductListXMLFailed
{
    NSLog(@"Product list updated failed ");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
