//
//  ProductCell.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
{
    UIImageView *productImg;
    UILabel *productNameLabel;
    UILabel *productOfferLabel;
    UILabel *noImageLabel;
    UIActivityIndicatorView *imgLoadingIndicator;
    UIView *headerContainerView;
    UIButton *removeFavoritebutton;
  
}

@property (nonatomic, strong) IBOutlet UIImageView *productImg;
@property (nonatomic, strong) IBOutlet UIImageView *accesoryImageView;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *productOfferLabel;
@property (nonatomic, strong) IBOutlet UILabel *noImageLabel;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *imgLoadingIndicator;
@property (nonatomic, strong) IBOutlet UIView *headerContainerView;
@property (strong, nonatomic) IBOutlet UILabel *noticeLabel;
@property (nonatomic, strong) IBOutlet UIButton *removeFavoritebutton;

@property (strong, nonatomic) IBOutlet UIImageView *msgImage;
@property (strong, nonatomic) IBOutlet UILabel *numberlabel;
@end
