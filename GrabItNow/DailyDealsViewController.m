//
//  DailyDealsViewController.m
//  GrabItNow
//
//  Created by MyRewards on 3/22/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "DailyDealsViewController.h"

#import "AppDelegate.h"


@interface DailyDealsViewController ()
{
    AppDelegate *appDelegate;
    ASIFormDataRequest *getImageNPidRequest;
    ASIFormDataRequest * productFetchRequest;
    ProductDataParser *productDataXMLParser;
    NSString *currentProID;
    int i;
}
- (void) showActivityView;
- (void) dismissActivityView;
-(void)fetchImageAndProductID;
@end

@implementation DailyDealsViewController

@synthesize activityView;
@synthesize productController;
@synthesize productDetailView;
@synthesize prodDetail;
@synthesize myImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    i=0;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSLog(@"%@",appDelegate.sessionUser.client_id);
    NSLog(@"%@",appDelegate.sessionUser.country);
    
    if(![appDelegate isIphone5])
    {
        CGRect myImageFrame = self.myImageView.frame;
        myImageFrame.size.height = 400;
        self.myImageView.frame = myImageFrame;
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        CGRect myImageFrame = self.myImageView.frame;
        myImageFrame.size.height = 520;
        self.myImageView.frame = myImageFrame;
        
        
    }
   /* self.myImageView.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height+20);*/
    [self showActivityView];
    [self fetchImageAndProductID];
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [appDelegate.homeViewController hideBackButon:YES];
    [appDelegate.homeViewController setHeaderTitle:@"Daily Deals"];
}

//============================ SHOW ALERTVIEW METHODS  =============================//
- (void) showActivityView {
    //activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}
- (void) dismissActivityView {
    [activityView removeFromSuperview];
}
//==================================================================================//


-(IBAction)Image_Clicked:(id)sender;
{
    [self showActivityView];
    [self fetchProductWithProductID:currentProID];
    
    
}

//============================ PRODUCT IMAGE AND ID FETCHING =======================//
-(void)fetchImageAndProductID {
    
   
    
    
    NSString *urlString = [[NSString stringWithFormat:@"http://www.myrewards.com.au/newapp/get_daily_deal.php?cid=%@&country=%@",appDelegate.sessionUser.client_id,appDelegate.sessionUser.country]stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is:==> %@",url);
  
    getImageNPidRequest = [[ASIFormDataRequest alloc] initWithURL:url];
   

    
    [getImageNPidRequest setDelegate:self];
    [getImageNPidRequest startAsynchronous];
    
    
    
}
//============================ PRODUCT PRODUTS ===================================//
- (void) fetchProductWithProductID:(NSString *) productId {
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    
    NSLog(@"Search URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    
}





//============================ ASIHTTP REQUEST DELEGATE METHODS ====================//
#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSLog(@"DailyDealsVC -- requestFinished:");
    
    if (request == getImageNPidRequest) {
       
        NSLog(@"**getImageNPidRequest  RES: %@",[request responseString]);        
        getImageNPidRequest = nil;
        i=1;
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        
        
    }
    if (request == productFetchRequest) {
        [self dismissActivityView];
        productFetchRequest = nil;
        i=2;
        NSLog(@"Product ** RES: %@",[request responseString]);
        NSLog(@"ProductDataParser");
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    NSLog(@"DailyDealVC -- requestFailed:");
    
    if (request == getImageNPidRequest) {
       // [self dismissActivityView];
         NSLog(@"**getImageNPidRequest  req failed: %@",[request responseString]);  
        getImageNPidRequest = nil;
    }
    else {
      //  [self dismissActivityView];
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];
        productFetchRequest = nil;
    }
}



//============================ ***********************   ==========================//

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product {
    
    prodDetail = product;
    // NSLog(@"\n product offer = %@", prodDetail.productOffer);
    
    if(i==1)
    {
        NSLog(@"%@",prodDetail.hotoffer_extension);
       NSLog(@"%@",prodDetail.productId);
        currentProID = prodDetail.productId;
        
        NSString *str = [NSString stringWithFormat:@"%@%@.%@",DailyDeal_Image_URL_Prefix ,prodDetail.productId,prodDetail.hotoffer_extension];
        NSLog(@"String is:::%@",str);
        
                                 
       UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:str]]];
        self.myImageView.image = image;
         [self dismissActivityView];
    }
    
    else if (i==2){
    
//    if (productController) {
//        if ([productController.view superview]) {
//            [productController.view removeFromSuperview];
//        };
//        productController = nil;
//    }
//    
//    ProductViewController *prVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil andProduct:prodDetail];
//    prVC.fromNearbyMe = YES;
//    prVC.delegate = self;
//    productController = prVC;
//    [self.navigationController pushViewController:prVC animated:YES];
//    
//    [productController updateUIWithProductDetails:product];
        
        if (productDetailView) {
            if ([productDetailView.view superview]) {
                [productDetailView.view removeFromSuperview];
            };
            productDetailView = nil;
        }
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        ProductDetailViewController *productDVC = [storyboard instantiateViewControllerWithIdentifier:@"productDetailViewController"];
        productDVC.product_ID = prodDetail.productId;
        [self.navigationController pushViewController:productDVC animated:YES];
        
        [productController updateUIWithProductDetails:product];

    }
}

-(Product *)returnProduct:(Product *)productDet
{
    return nil;
}

- (void)parsingProductDataXMLFailed {
    
}




//==================================================================================================//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
