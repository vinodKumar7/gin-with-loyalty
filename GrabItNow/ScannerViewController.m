//
//  ScannerViewController.m
//  CWE
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "ScannerViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
@interface ScannerViewController ()
{
    AppDelegate *appDelegate;

}

@end

@implementation ScannerViewController
@synthesize myImageView;
@synthesize mytextView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)Scan_Button_Action:(id)sender {
    
    NSLog(@"TBD: scan barcode here...");
    
    // ADD: present a barcode reader that scans from the camera feed
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    //  [self presentModalViewController: reader
    // animated: YES];
    [appDelegate.homeViewController presentViewController:reader animated:YES completion:NULL];
    [appDelegate.homeViewController.headerView setHidden:YES ];
    [appDelegate.homeViewController.bannerView setHidden:YES];
    
    
    
}

- (IBAction)closeWindow:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];}
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
     mytextView .text = symbol.data;
   // mytextView .text = symbol.typeName;
    
    
    // EXAMPLE: do something useful with the barcode image
    myImageView.image =
    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    //[reader dismissModalViewControllerAnimated: YES];
    
    [reader dismissViewControllerAnimated:YES completion:NULL];
   

    
    
    if([symbol.typeName isEqualToString:@"QR-Code"]){
        
        NSURL *url = [NSURL URLWithString:symbol.data];
        
    //    [[UIApplication sharedApplication] openURL:websiteUrl];
   ASIFormDataRequest   *redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
        [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
        [redeemRequest setPostValue:@"1023391" forKey:@"pid"];
        [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
        [redeemRequest setPostValue:@"0.00" forKey:@"lat"];
        [redeemRequest setPostValue:@"0.00" forKey:@"lon"];
        NSLog(@"User ID %@",appDelegate.sessionUser.userId );
        
       // NSLog(@"Pro Id %@",redeemProductId);
        NSLog(@"client ID %@",appDelegate.sessionUser.client_id );
        [redeemRequest setDelegate:self];
        [redeemRequest startAsynchronous];

    
    
    }
    
    else if([symbol.typeName isEqualToString:@"EAN-13"]){
        
        
        
        
        
    }
    
    
    
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)loadviews
{

    if ([appDelegate isIphone5] )
    {
        CGRect frame=self.myImageView.frame;
        NSLog(@"frame    origin is %f ",frame.origin.y);
        self.myImageView.frame=frame;
        CGRect frame1=self.mytextView.frame;
        NSLog(@"frame one  origin is %f ",frame1.origin.y);
        
        self.mytextView.frame=frame1;
        CGRect frame2=self.scan_Button.frame;
        NSLog(@"frame two  origin is %f ",frame2.origin.y);
        
        self.scan_Button.frame=frame2;
        
        
        
    }
    else
    {
        CGRect frame=self.myImageView.frame;
        NSLog(@"frame    origin is %f ",frame.origin.y);
        self.myImageView.frame=frame;
        CGRect frame1=self.mytextView.frame;
        NSLog(@"frame one  origin is %f ",frame1.origin.y);
        
        self.mytextView.frame=frame1;
        CGRect frame2=self.scan_Button.frame;
        NSLog(@"frame two  origin is %f ",frame2.origin.y);
        
        self.scan_Button.frame=frame2;

    
    }


}
-(void) viewWillAppear:(BOOL)animated
{   [self loadviews];

    [appDelegate.homeViewController setHeaderTitle:@"Scan QRCode"];
    [appDelegate.homeViewController.headerView setHidden:NO];
    [appDelegate.homeViewController.bannerView setHidden:NO];
    
   
    [self.navigationController setNavigationBarHidden:YES animated:NO];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)requestFinished:(ASIHTTPRequest *)request
{ NSLog(@"result for zbar code %@ ",[request responseString]);
    UIAlertView *updateAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"Success" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [updateAlert show];
   
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView *updateAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter valid coupon code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [updateAlert show];
    NSLog(@"result failed for zbar code %@ ",[request responseString]);
}
- (void)viewDidUnload {
    [self setScan_Button:nil];
    [super viewDidUnload];
}
@end
