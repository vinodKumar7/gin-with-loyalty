//
//  CollectionViewManager.m
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by Daniel García on 31/12/13.
//  Copyright (c) 2013 Produkt. All rights reserved.
//

#import "CollectionViewManager.h"
#import <AdSupport/ASIdentifierManager.h>

#import "CollectionViewCell.h"
#import "CollectionViewSectionHeader.h"
#import "DMLazyScrollView.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "Merchant.h"
#import "Product.h"
#import "Redeem.h"

#import "CollectionViewManagerCell.h"


static NSUInteger const kNumberOfSections = 2;
static NSUInteger const kNumberItemsPerSection = 1;
@interface CollectionViewManager()<UIScrollViewDelegate, DMLazyScrollViewDelegate>
{
    float stampWidth;
    float stampHeight;
    float totalStamps;
    UIView *stampIconContainer;

    UIScrollView* cellScrollView;
    UIButton *reduceCellSizeButton;
    UIButton *expandCellSizeButton;
    UILabel *proOfferLabel;
    
    UIButton *btnHeader;
    NSInteger currentBtnTag;
    int xOrigin, contactLabelOrigin_Y;
    NSString *callNumber;
    
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    UIWebView   *helpWebView;
    NSString *htmlString;
    NSInteger previousSelection;
    
    UIButton *currentButton;
    UIButton *previousButton;
    UIView *stampsView, *contactsView;
    UITableView *addressTableView;
    
    int xOriginStamp, yOriginStamp, tempOrigin;
    
    ASIFormDataRequest *getProductRequest;
    ASIFormDataRequest *redeemstatusRequest;
    ASIFormDataRequest *redeemRequest;
    ASIFormDataRequest * merchantFetchRequest;
    ASIFormDataRequest * freebiesRequest;
    RedeemXMLparser *redeemparser;
    ProductDataParser *productDataXMLParser;
    Product *currentProduct;
    Redeem *currentRedeem;
    
    NSMutableArray *merchantList;
    
    Merchant *merchant;
    MerchantListParser *merchantListParser;
    
    BOOL isFromParsingCountFinished;
    BOOL isThroughKeyboardView;
    
    
    //int originalRedeemCount;
    AppDelegate *appDelegate;
    
    
}
@property (strong,nonatomic) UINib *cellNib;
@property (strong,nonatomic) UINib *sectionHeaderNib;


@end
@implementation CollectionViewManager
@synthesize proImage, selectedStampCount, redeem_psw, redeem_Button,stampTapCount, checkedStamps, redeem_Target,OProduct,freebiesButtonCM;

- (void)setCollectionView:(UICollectionView *)collectionView{
    _collectionView = collectionView;
    if (_collectionView) {
        [self initCollectionView:collectionView];
       
    }
}
- (void)initCollectionView:(UICollectionView *)collectionView{
    collectionView.dataSource=self;
    collectionView.delegate=self;
    
    [self registerCellsForCollectionView:collectionView];
    [self registerSectionHeaderForCollectionView:collectionView];
    [collectionView reloadData];
   
    
    currentBtnTag = 1;
    stampTapCount = 0;
    isThroughKeyboardView = NO;
    selectedStampCount = [NSString stringWithFormat:@"%d",1];
    currentProduct = OProduct;
    previousButton = [[UIButton alloc]init];
    previousButton.tag = 5;
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
      NSLog(@"Oproduct is %@",OProduct);
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix_Loyalty,_product_ID];
    NSLog(@"URL is %@ ",urlString);
    NSURL *url= [NSURL URLWithString:urlString];
    getProductRequest = [ASIFormDataRequest requestWithURL:url];
    [getProductRequest setDelegate:self];
    [getProductRequest startAsynchronous];
    
    [self fetchMerchantWithProductID:currentProduct.productId];
   

}

#pragma mark - Cells

- (UINib *)cellNib{
    if (!_cellNib) {
        _cellNib = [UINib nibWithNibName:@"CollectionViewCell" bundle:nil];
        
    }
    return _cellNib;
}

- (UINib *)sectionHeaderNib{
    if (!_sectionHeaderNib) {
        _sectionHeaderNib = [UINib nibWithNibName:@"CollectionViewSectionHeader" bundle:nil];
    }
    return _sectionHeaderNib;
}

- (void)registerCellsForCollectionView:(UICollectionView *)collectionView{
    [collectionView registerNib:self.cellNib forCellWithReuseIdentifier:@"CollectionViewCell"];
}

- (void)registerSectionHeaderForCollectionView:(UICollectionView *)collectionView{
    [collectionView registerNib:self.sectionHeaderNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewSectionHeader"];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return kNumberOfSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return kNumberItemsPerSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    CollectionViewCell *cell;
    static NSString *cellIdentifier = @"CollectionViewCell";
    cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CollectionViewManagerCell"];
    
    CollectionViewManagerCell *cell1;
    static NSString *cellIdentifier1 = @"CollectionViewManagerCell";
    cell1 = [self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier1 forIndexPath:indexPath];
    
    
    //if (cell == nil)
    //        cell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
   // cell.imageView.image =nil;
    if (indexPath.section == 0) {
        
        cell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if(self.proImage)
            NSLog(@"proimage");
        else
            NSLog(@"no proimage");
        
        cell.proImageView.image = self.proImage;
        cell.proImageView.layer.borderWidth=2.0;
        cell.proImageView.layer.borderColor=[UIColor whiteColor].CGColor;
        
        cell.titleLabel.text=currentProduct.productName;
        cell.subtitleLabel.text=currentProduct.productOffer;
        
      //  [self updateFavoriteStatus];
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        cell1 = (CollectionViewManagerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier1 forIndexPath:indexPath];
        
        //cell.bgImageView.hidden = YES;
        [cell1 addSubview:lazyScrollView];
        
    }
    
    return cell1;





}





- (void)collectionView:(UICollectionView *)myCollectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath layout:(UICollectionViewLayout*)collectionViewLayout
{
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewSectionHeader *sectionHeaderView;
    static NSString *viewIdentifier=@"CollectionViewSectionHeader";
    sectionHeaderView=[self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:viewIdentifier forIndexPath:indexPath];
    NSString *sectionHeaderTitle=[NSString stringWithFormat:@"Section %ld",(long)indexPath.section];
    
    
    if(indexPath.section==0)
    {[self shouldStickHeaderToTopInSection:0];
        sectionHeaderTitle=[sectionHeaderTitle stringByAppendingString:@" (should not stick to top)"];
    }
    
  
    if (indexPath.section==1)
    {
        
        if(appDelegate.isFromFreebies){
            
            sectionHeaderView.containerView.hidden = YES;
            sectionHeaderView.freebiesLabel.hidden = NO;
            lazyScrollView.scrollEnabled           = NO;
            
        }
        else{
        [sectionHeaderView.button1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [sectionHeaderView.button2 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [sectionHeaderView.button3 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [sectionHeaderView.button4 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [sectionHeaderView.button5 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    return sectionHeaderView;
}

-(UIView *)getSectionHeaderView{
    
    UIView *myView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    myView.backgroundColor = [UIColor lightGrayColor];
    xOrigin = 15;
    for (int i = 1; i <= 5; i++) {
        
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(xOrigin, 7, 37, 37)];
        btn.tag = i;
        btn.layer.cornerRadius = 27.0;
        [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn%d.png",i]] forState:UIControlStateNormal];
        
        //btn.backgroundColor = [UIColor redColor];
        [btn addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [myView addSubview:btn];
        
        xOrigin = xOrigin + 60;
    }
    
    return myView;
    
    
}

#pragma mark - UICollectionViewDelegate

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0)
    
        
        return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height/2.5);

    
    
    else  if (indexPath.section==1)
    {
        if (isFromParsingCountFinished == YES) {
          
            
            int height;
            switch (currentBtnTag) {
                case 1:
                    return CGSizeMake(self.collectionView.bounds.size.width, 500);
                    break;
                case 2:
                
                    height = [self getStampsViewHeight];
                    return CGSizeMake(self.collectionView.bounds.size.width, height);
                
                    break;
                case 3:
                
                     height = [self getStampsViewHeight];
                    return CGSizeMake(self.collectionView.bounds.size.width, height);
                
                    break;
                case 4:
                
                     height = [self getStampsViewHeight];
                    return CGSizeMake(self.collectionView.bounds.size.width, height);
                
                    break;
                case 5:
                
                     height = [self getStampsViewHeight];
                    return CGSizeMake(self.collectionView.bounds.size.width, height);
                
                    break;
                    
                default:
                    break;
            }
           
            
        }

    }
    
    

    
    return CGSizeMake(0, 0);
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1.0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (section==0)
        return CGSizeMake(self.collectionView.bounds.size.width, 0);
    else
    {
        if([[ UIScreen mainScreen] bounds].size.height == 480)
        
            return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height/6.5);
        
        else if ([[ UIScreen mainScreen] bounds].size.height == 568 || [[ UIScreen mainScreen] bounds].size.height == 667)
        
            return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height/8.1);
        
        else
            return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height/8.5);
    }
    
}

#pragma mark- User Defined Methods

-(int)getStampsViewHeight{
    
    
    if( [ [ UIScreen mainScreen ] bounds ].size.height == 480)
        return 145;
    else if( [ [ UIScreen mainScreen ] bounds ].size.height == 568)
        return 195;
    else if( [ [ UIScreen mainScreen ] bounds ].size.height == 667)
        return 240;
    else
        return 280;
    
    
}

- (void)pageSetting
{
   

    
    // PREPARE PAGES
    NSUInteger numberOfPages = 5;
    [viewControllerArray removeAllObjects];
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    lazyScrollView = Nil;
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height)];
    lazyScrollView.tag = 1222;
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
    
    return [self controllerAtIndex:index];
        
   
        
    };
    
    
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [self.collectionView.collectionViewLayout invalidateLayout];
}


- (IBAction)btnPressed:(id)sender
{
    
    //NSLog(@"self.collectionView.contentOffset.y:: %f is %ld", self.collectionView.contentOffset.y,(long)currentBtnTag);
    currentBtnTag = [sender tag];
    [self.collectionView.collectionViewLayout invalidateLayout];
    
    if(self.collectionView.contentOffset.y>0)
    {
        
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
    currentButton = sender;
    
    [self setHighlightButtonImage:[sender tag]];
    
    if (previousSelection < [currentButton tag])
        [lazyScrollView setPage:[currentButton tag]-1 transition:FORWARD animated:NO];
    else
        [lazyScrollView setPage:[currentButton tag]-1 transition:BACKWARD animated:NO];
    
    
}

-(void)setHighlightButtonImage:(NSInteger) index
{
    [self.collectionView.collectionViewLayout invalidateLayout];
    
   
    UIButton     *btnHeader333 = (UIButton *)[_collectionView viewWithTag:previousButton.tag];
    [btnHeader333 setImage: [[UIImage imageNamed: [NSString stringWithFormat:@"btn%ld.png",(long)previousButton.tag]] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] forState: UIControlStateNormal];
    
    UIButton     *btnHeader444 = (UIButton *)[_collectionView viewWithTag:currentBtnTag];
    [btnHeader444 setImage: [[UIImage imageNamed: [NSString stringWithFormat:@"btnSelected%ld.png",(long)currentBtnTag]] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] forState: UIControlStateNormal];
    
    previousButton.tag=currentBtnTag;
    
}
- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
    //NSLog(@"---------------------controllerAtIndex %ld-------------------", (long)index);
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor whiteColor];
        
        switch (index) {
            case 0:
                [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productDesciption andHeadding:currentProduct.productOffer]];
                break;
            case 1:
                if ([merchantList count] == 0)
                    [contr.view addSubview:helpWebView];
                
                else {
                    addressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.collectionView.bounds.size.width, 266.0f) style:UITableViewStylePlain];
                    addressTableView.backgroundColor = [UIColor whiteColor];
                    addressTableView.dataSource = self;
                    addressTableView.delegate = self;
                    [contr.view addSubview:addressTableView];
                }
                
                break;
            case 2:
                
                contactsView= [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f, self.collectionView.bounds.size.width, 266.0f)];
                contactsView.backgroundColor = [UIColor whiteColor];
                [contr.view addSubview:contactsView];
                [self updateContactsView];
                
                break;
            case 3:
                
                if([currentProduct.productTermsAndConditions length]>0)
                    [contr.view addSubview:[self prepareTextToDisplay:currentProduct.productTermsAndConditions andHeadding:@"Terms And Conditions"]];
                else
                    [contr.view addSubview:[self prepareTextToDisplay:@"" andHeadding:@"No Terms And Conditions"]];
                
                
                break;
            case 4:
                
                stampsView= [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,self.collectionView.bounds.size.width, [self getStampsViewHeight])];
                [contr.view addSubview:stampsView];
                
                
                if (appDelegate.isFromFreebies)
                {
                    NSLog(@"PRESENTING FREEBIE VIEW....");
                    [stampsView addSubview:[self getFreebieView]];
                    [redeem_Button setTitle:@"OR CLICK HERE TO USE NOW" forState:UIControlStateNormal];
                    [self insertingFreebies];
                }
                else
                {
                    
                totalStamps = [currentProduct.redeemTarget intValue];
                checkedStamps = [currentProduct.currentRedeemCount intValue];
                [stampsView addSubview:[self getProductOfferLabel]];
                
                
                   
                int firstRowStampsCount =  (totalStamps > 3)? ceil(totalStamps/2):totalStamps;
                
                
                
                float availableContainerWidth = stampsView.frame.size.width - 2* DEFAULTSPACE -(firstRowStampsCount-1)*DEFAULTSPACE;
                
        
                stampIconContainer = [[UIView alloc]initWithFrame:CGRectMake(0, FREEMESSAGEDISPLAYLBLHGT + DEFAULTSPACE, stampsView.frame.size.width, stampsView.frame.size.height - (FREEMESSAGEDISPLAYLBLHGT + DEFAULTSPACE))];
                stampIconContainer.layer.borderColor  = [UIColor blackColor].CGColor;
                stampIconContainer.clipsToBounds      = YES;
                
                
                
                float totalSpace = (availableContainerWidth > stampIconContainer.frame.size.height)? stampIconContainer.frame.size.height : availableContainerWidth;
                
                float eachStampSize;
                if(firstRowStampsCount < totalStamps) //multi line stamps
                    eachStampSize = (totalSpace - HORIZONTALDEFAULTSPACE)/2;
                else
                    eachStampSize = (totalSpace - HORIZONTALDEFAULTSPACE);
                   
                if(eachStampSize*firstRowStampsCount < availableContainerWidth)
                   stampHeight = stampWidth = eachStampSize;
                else
                    {
                    float extraSize =  eachStampSize*firstRowStampsCount - availableContainerWidth;
                    stampHeight = stampWidth = eachStampSize-extraSize/firstRowStampsCount;
                    }
                
                
                
                int rows = (totalStamps > firstRowStampsCount)?2:1;
                
                CGRect frame      = stampIconContainer.frame;
                frame.size.width  = stampWidth*firstRowStampsCount+(firstRowStampsCount -1)*DEFAULTSPACE;
                frame.size.height = stampHeight*rows+2;
                frame.origin.x    = (stampsView.frame.size.width - frame.size.width)/2;
                frame.origin.y    = (stampsView.frame.size.height-frame.size.height)/2 + 5;
                stampIconContainer.frame = frame;
                
               
                

                
                int stampXPosition = 0; //DEFAULTSPACE;
                
              
               [self addIntailStamp:1 endStamp:firstRowStampsCount stampXPosition:stampXPosition stampYPosition:0 checkedStamps:checkedStamps];
                
                if(totalStamps == 5 ||totalStamps == 7 || totalStamps == 9)
                    stampXPosition = stampXPosition + stampWidth/2;
                else
                    stampXPosition = stampXPosition;
                
                
                 [self addIntailStamp:firstRowStampsCount+1 endStamp:totalStamps stampXPosition:stampXPosition stampYPosition:stampHeight+2 checkedStamps:checkedStamps];
                
                [stampsView addSubview:stampIconContainer];
                }
                break;
                
            default:
                break;
        }
        
        
        
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}
-(void)insertingFreebies
{

    NSString *urlString = [NSString stringWithFormat:@"http://184.107.152.53/newapp/myfreebies.php"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"image extension is %@",OProduct.appImageExtension);
    //  NSLog(@"url is %@ product_ID is %@ device ID id %@",url,product_ID,deviceID);
    freebiesRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [freebiesRequest setPostValue:@"2A240C91-C641-45CD-A8B7-6F96B3498A9D" forKey:@"uid"];
    [freebiesRequest setPostValue:_product_ID                              forKey:@"pid"];
    [freebiesRequest setPostValue:@"24"                                   forKey:@"cid"];
    [freebiesRequest setPostValue:@"1"                                    forKey:@"quantity"];
    [freebiesRequest setPostValue:currentProduct.productDesciption              forKey:@"pdesc"];
    [freebiesRequest setPostValue:@"insert"                               forKey:@"function"];
    [freebiesRequest setPostValue:currentProduct.productName                    forKey:@"pname"];
    [freebiesRequest setPostValue:currentProduct.redeemPassword                 forKey:@"reedem_password"];
    [freebiesRequest setPostValue:currentProduct.appImageExtension              forKey:@"image_ext"];
    [freebiesRequest setPostValue:currentProduct.productOffer                   forKey:@"highlight"];
    [freebiesRequest setDelegate:self];
    [freebiesRequest startAsynchronous];

}
-(UIWebView *)prepareTextToDisplay:(NSString *)content andHeadding: (NSString *)header{
    
    
    UIWebView  *containerWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f, self.collectionView.bounds.size.width, 566.0f)];
    
    containerWebView.userInteractionEnabled = NO;
    containerWebView.backgroundColor = [UIColor whiteColor];
    
    NSString *resultText = @"";
    
    resultText =       [NSString stringWithFormat:@"<html> \n"
                        
                        "<head> \n"
                        "<style type=\"text/css\"> \n"
                        "body {font-family: \"%@\"; font-size: \"%@ \";}\n"
                        "H4{ color: rgb(198,38,21) }\n"
                        "</style> \n"
                        "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                        "</head> \n"
                        "<body><H4>%@</H4>%@</body> \n"
                        "</html>", @"Helvetica", [NSNumber numberWithInt:10],header, content];
    
    [containerWebView loadHTMLString:resultText baseURL:nil];
    return containerWebView;
}
- (UIView * )getFreebieView {
    
    UIView *freebiesView= [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,self.collectionView.bounds.size.width, [self getStampsViewHeight])];
    
    UILabel *congratsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, freebiesView.bounds.size.width, 30)];
    [congratsLabel setFont:[UIFont fontWithName:@"Helvetica" size:24]];
    congratsLabel.textAlignment = NSTextAlignmentCenter;
    [congratsLabel setTextColor:[UIColor blackColor]];
    congratsLabel.text = @"Congratulations!";
    [freebiesView addSubview:congratsLabel];
    
    UILabel *yourNextLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, freebiesView.bounds.size.width, 30)];
    [yourNextLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    yourNextLabel.textAlignment = NSTextAlignmentCenter;
    [yourNextLabel setTextColor:[UIColor blackColor]];
    yourNextLabel.text = @"your next coffee";
    [freebiesView addSubview:yourNextLabel];
    
    UILabel *isFreeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, freebiesView.bounds.size.width, 30)];
    [isFreeLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    isFreeLabel.textAlignment = NSTextAlignmentCenter;
    [isFreeLabel setTextColor:[UIColor blackColor]];
    isFreeLabel.text = @"is Free!";
    [freebiesView addSubview:isFreeLabel];
    
    
    UILabel *yourRewardLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, isFreeLabel.frame.origin.y+15, freebiesView.frame.size.width-50, 60)];
    [yourRewardLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    yourRewardLabel.numberOfLines=2;
    yourRewardLabel.backgroundColor=[UIColor clearColor];
    yourRewardLabel.textAlignment=NSTextAlignmentCenter;
    yourRewardLabel.textColor=[UIColor redColor];
    yourRewardLabel.text=@"Your reward has been saved to My freebies";
    [freebiesView addSubview:yourRewardLabel];
    
 //   Product *pro = [productsList objectAtIndex:indexPath.row];//isFreeLabel.frame.size.height-70
    freebiesButtonCM.frame = CGRectMake(freebiesView.frame.size.width-45,60 , 40, 40);
    [freebiesButtonCM setBackgroundImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    //freebiesButtonCM.layer.backgroundColor=[UIColor blackColor].CGColor;
    //freebiesButtonCM.layer.borderWidth=2.0;
    [freebiesView addSubview:freebiesButtonCM];
    
    NSLog(@"freebies Button is %@ ",freebiesButtonCM);
    
    return freebiesView;
    
    
}


- (void)lazyScrollViewDidEndDecelerating:(DMLazyScrollView *)pagingView atPageIndex:(NSInteger)pageIndex
{

 
     UIButton *button=[[UIButton alloc]init];
    button.tag=pageIndex+1;
    
     [self performSelector:@selector(btnPressed:) withObject:button afterDelay:0.03];

}

-(UILabel *)getProductOfferLabel{
    
    
    proOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 4, stampsView.bounds.size.width, 15)];
    [proOfferLabel setFont:[UIFont fontWithName:@"Helvetica" size:13]];
    
    proOfferLabel.text = [NSString stringWithFormat:@"Receive the %dth coffee For Free!",(int)totalStamps];
    
    proOfferLabel.textAlignment = NSTextAlignmentCenter;
    [proOfferLabel setTextColor:[UIColor redColor]];
    return proOfferLabel;
}

- (void)lazyScrollViewDidEndDragging:(DMLazyScrollView *)pagingView{
    
    [self.collectionView.collectionViewLayout invalidateLayout];
   
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex{
    
    
   // NSLog(@"currentPageIndex::%ld", (long)currentPageIndex);
    currentBtnTag = currentPageIndex+1;
    
   // NSLog(@"lazyscroll view width is %f",pagingView.bounds.size.width);
}

- (void) updateContactsView {
    
    NSString* phnoString = [currentProduct.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    phArray = [[NSArray alloc]init];
    phMutableArray = [[NSMutableArray alloc]init];
    
    //    phArray = [phnoString componentsSeparatedByString:@"/,"];
    phArray = [phnoString componentsSeparatedByCharactersInSet:
               [NSCharacterSet characterSetWithCharactersInString:@"/,"]
               ];
    NSLog(@"strings %@",phArray);
    
    if([phArray count] !=  0)
    {
        contactLabelOrigin_Y = 10;
        for (int i = 0; i < [phArray count]; i++) {
            
            UILabel *contactlabel = [[UILabel alloc]initWithFrame:CGRectMake(60, contactLabelOrigin_Y, 150, 30)];
            [contactsView addSubview:contactlabel];
            
            if ([[phArray objectAtIndex:i] length] <= 4) {
                
                NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]];
                
                for (int j=0; j < [[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]; j++) {
                    NSString *ichar  = [NSString stringWithFormat:@"%c", [[phArray objectAtIndex:i-1] characterAtIndex:j]];
                    [characters addObject:ichar];
                }
                
                NSString * newString = [[characters valueForKey:@"description"] componentsJoinedByString:@""];
                contactlabel.text = [newString stringByAppendingString:[phArray objectAtIndex:i]];
                
                [phMutableArray addObject:contactlabel.text];
            }
            else
            {
                contactlabel.text = [phArray objectAtIndex:i];
                [phMutableArray addObject:[phArray objectAtIndex:i]];
            }
            
            
            UIButton *contactlabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(30, contactLabelOrigin_Y+3, 25, 25)];
            contactlabel_Button.tag = i;
            NSLog(@"contact btn tag %ld",(long)[contactlabel_Button tag]);
            [contactlabel_Button setBackgroundImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
            [contactsView addSubview:contactlabel_Button];
            
           
            [contactlabel_Button addTarget:self action:@selector(callButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
            contactlabel_Button.userInteractionEnabled = YES;
            
            
            
            //            [contactBtn setBackgroundImage:[UIImage imageNamed:@"btn3.png"] forState:UIControlStateNormal];
            
            
            
            contactLabelOrigin_Y = contactLabelOrigin_Y + 30;
        }
        
    }
    else
    {
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];
        
    }
    
}

-(IBAction)callButtonpressed:(id)sender
{
    NSLog(@"call button pressed");
    
    
    callNumber = [phMutableArray objectAtIndex:[sender tag]];
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    call_Alert.tag = 111;
    [call_Alert show];

    
}
-(void)addIntailStamp:(int)start endStamp:(int)end stampXPosition:(int)xPosition stampYPosition:(int)yPosition checkedStamps:(int)checkedStampsCount{
    
    
    for (int stamp = start; stamp <= end; stamp++) {
        
        UIImageView *stampView = [[UIImageView alloc]initWithFrame:CGRectMake(xPosition, yPosition, stampWidth, stampHeight)];
        //stampView.layer.borderColor  = [UIColor blackColor].CGColor;
        //stampView.layer.borderWidth = 0.5;
        
        
        if(stamp == totalStamps)
            stampView.image = [UIImage imageNamed:@"free.png"];
        else if (checkedStamps >= stamp)
            stampView.image = [UIImage imageNamed:@"round_fill.png"];
        else{
            stampView.image = [UIImage imageNamed:@"round_empty.png"];
        
            UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(handleSingleTap:)];
            stampView.tag=stamp+100;
            
            if (stamp==checkedStampsCount+1)
                   stampView.userInteractionEnabled=YES;
            
            
            [stampView addGestureRecognizer:singleFingerTap];
        }
        
        [stampIconContainer addSubview:stampView];
        
        
        if(stamp != end)
        {
            
            UIImageView *conjectionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"+.png"]];
            conjectionView.center = CGPointMake(stampView.frame.origin.x+stampWidth+DEFAULTSPACE/2, stampView.frame.origin.y+stampHeight/2);
            if(stamp == totalStamps-1)
                conjectionView.image = [UIImage imageNamed:@"=.png"];
            else
                conjectionView.image = [UIImage imageNamed:@"+.png"];
            [stampIconContainer addSubview:conjectionView];
        }
        
        xPosition = stampView.frame.origin.x+ stampView.frame.size.width + DEFAULTSPACE;
        
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleSingleTap");
    
//    if (recognizer.view.tag -100 == [currentProduct.redeemTarget integerValue]-1){
//        NSLog(@"-------------------FREEBIE-------------------");
//        isFreebie = YES;
//        appDelegate.isFromFreebies = YES;
//    }
    
    
    int present   = (int)recognizer.view.tag;
    int forward   = (int)recognizer.view.tag+1;
    int backward  = (int)recognizer.view.tag-1;
    
    UIImageView *presentImageView  = (UIImageView *)[stampIconContainer viewWithTag:present];
    UIImageView *backwardImageView = (UIImageView *)[stampIconContainer viewWithTag:backward];
    UIImageView *forwardImageView  = (UIImageView *)[stampIconContainer viewWithTag:forward];
    
    
    NSData *imageData1 = UIImagePNGRepresentation(presentImageView.image);
    NSData *imageData2 = UIImagePNGRepresentation([UIImage imageNamed:@"round_selected.png"]);
    
    if ([imageData1 isEqual:imageData2])
    {
        presentImageView.image = [UIImage imageNamed:@"round_empty.png"];
        backwardImageView.userInteractionEnabled = YES;
        presentImageView.userInteractionEnabled  = YES;
        forwardImageView.userInteractionEnabled  = NO;
        stampTapCount--;
        
        
    }
    else
    {
        stampTapCount++;
        
//        if ([currentProduct.redeemTarget integerValue]==stampTapCount+checkedStamps+1)
//            stampTapCount++;
        
        
        presentImageView.image = [UIImage imageNamed:@"round_selected.png"];
        backwardImageView.userInteractionEnabled = NO;
        presentImageView.userInteractionEnabled  = YES;
        forwardImageView.userInteractionEnabled  = YES;
        
        
    }
    
    selectedStampCount = [NSString stringWithFormat:@"%d",stampTapCount];
     NSLog(@"stampTapCount:: %d",stampTapCount);
    NSLog(@"selectedStampCount:: %@",selectedStampCount);
    
}

//--------------------------------------------------------------------------------------
#pragma mark- Redeem Button Pressed
-(void)redeemButtonPressed
{
  NSLog(@"redeemButtonPressed action....");
   isThroughKeyboardView = YES;
   [self redeemStatusRequestMethod];
    
    
}
//--------------------------------------------------------------------------------------



#pragma mark- Request Finished and Request Failed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if(request == getProductRequest){
        NSLog(@"=======>> Product:: %@",[request responseString]);
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        
    }
    
    else if (request == redeemstatusRequest)
    {
        NSLog(@"redeemstatus response %@ ",[request responseString]);
        NSXMLParser *countParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        redeemparser=[[RedeemXMLparser alloc]init];
        redeemparser.delegate = self;
        countParser.delegate=redeemparser;
        [countParser parse];
        //[activityView removeFromSuperview];
    }
    else if (request == freebiesRequest)
    {
        NSLog(@"freebiesRequest ** RES: %@",[request responseString]);
        
    }
    
    else  {
        NSLog(@"Merchant ** RES: %@",[request responseString]);
        
          NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
         merchantListParser = [[MerchantListParser alloc] init];
         merchantListParser.delegate = self;
         merchantParser.delegate = merchantListParser;
         [merchantParser parse];
         
         merchantFetchRequest = nil;
         // addressDetailsLoaded = YES;
    }
    
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:@"Cancel",nil];
    [alert_view show];
    
   
    
}


#pragma mark- fetch Address

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,_product_ID];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    merchantFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [merchantFetchRequest setDelegate:self];
    [merchantFetchRequest startAsynchronous];
    
    
}

#pragma mark- Parsing Methods

- (void)parsingProductDataFinished:(Product *)product
{
       currentProduct = product;
    redeem_psw     = currentProduct.redeemPassword;
    redeem_Target   = [currentProduct.redeemTarget intValue];
    [self redeemStatusRequestMethod];
    NSLog(@"product is %@ [currentProduct.redeemTarget intValue] is %d",product,[currentProduct.redeemTarget intValue]);

    
}

- (void)parsingProductDataXMLFailed
{
    
}

- (void) parsingcountFinished:(Redeem *) redeemObj
{
    
    int originalRedeemCount = [redeemObj.count intValue];
    NSLog(@"--------------------------------------------------------------------------------------");
    NSLog(@"originalRedeemCount is::: %d ------- [currentProduct.redeemTarget intValue] is:: %d ",originalRedeemCount,[currentProduct.redeemTarget intValue]);
    
    
    
    
    int redeemTarget;
    if(originalRedeemCount >= [currentProduct.redeemTarget intValue])
    {
        redeemTarget = [currentProduct.redeemTarget intValue];
        
        while (originalRedeemCount >= redeemTarget)
            originalRedeemCount = originalRedeemCount - redeemTarget;
        
        
    }
    
    NSLog(@" modified RedeemCount::%d",originalRedeemCount);
    currentProduct.currentRedeemCount = [NSString stringWithFormat:@"%d", originalRedeemCount];
    
    
    [self pageSetting];
    
    isFromParsingCountFinished = YES;
    [_collectionView reloadData];
    
    
    [lazyScrollView setPage:4 transition:FORWARD animated:NO];
    
   
}
- (void) RedeemXMLparsingFailed{}

-(void)redeemStatusRequestMethod{
    
    NSString *rdmurl =[NSString stringWithFormat:@"%@redeem_status.php?user_id=%@&pid=%@",URL_Prefix_Loyalty,[self deviceUDID],currentProduct.productId];
    NSURL *redeemurl= [NSURL URLWithString:rdmurl];
    redeemstatusRequest=[ASIFormDataRequest requestWithURL:redeemurl];
    [redeemstatusRequest setDelegate:self];
    [redeemstatusRequest startAsynchronous];

    
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    
    
    if (!merchantList)
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    
    else
        [merchantList addObjectsFromArray:merchantsListLocal];
        
    [addressTableView reloadData];
}


- (void)parsingMerchantListXMLFailed {
    
}

#pragma mark- AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 111) {
        NSString *phoneURLString = callNumber;
        NSString *newString = [ phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSURL *phoneURL = [NSURL URLWithString:newString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}


#pragma mark - PDKTStickySectionHeadersCollectionViewLayoutDelegate
- (BOOL)shouldStickHeaderToTopInSection:(NSUInteger)section{
    // Every section multiple of 3 doesn't stick to top
    return (section>0 && section%3==0)?NO:YES;
  /*  if (section==0)
    {
        return 0;
    }
    else
        return 1;*/
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [merchantList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    
    AddressCell *cell = (AddressCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        
        [[NSBundle mainBundle] loadNibNamed:@"AddressCell" owner:self options:nil];
        cell = self.cCell;
        
    }
    

    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    

    if([aMerchant.mMailAddress1 length] == 0  && aMerchant.mState.length ==0){
        
        cell.lblAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
//        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str =aMerchant.mMailAddress1 ;
        
        if( [aMerchant.mMailSuburb isEqualToString:@"(null)"] )
        {
            // NSLog(@"Address: %@",aMerchant.mMailAddress1);
            // str=@"trimmed";
            
        }
        else
        {
            NSLog(@"null: %@",[NSNull null]);
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        }
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        // NSString *deviceName = @"Kenny's iPhone";
        NSLog(@"non--stripped1 is...%@ ",str  );
        NSString *stripped = [str stringByReplacingOccurrencesOfString:@"(null)," withString:@""];
        NSString *stripped1 = [stripped stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        cell.lblAddress.text = stripped1;
        
    }
    cell.lblAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    cell.mapButton.tag=indexPath.row;
    [cell.mapButton addTarget:self action:@selector(mapButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
    cell.mapButton.userInteractionEnabled = YES;
    if ( (Location.latitude == 0.000000)|| (Location.longitude == 0.000000) || [cell.lblAddress.text isEqualToString:@"No Addresses Available"])
    {
        if (indexPath.row > 0) {
//            cell.lblAddress.text = @"";
        }
        cell.mapButton.hidden = YES;
//        cell.mapLabel.hidden = YES;
        
        
    }
    else
    {
        cell.mapButton.hidden = NO;
//        cell.mapLabel.hidden = NO;
        
        
    }
//    cell.mapButton.tag = indexPath.row;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(IBAction)mapButtonpressed:(id)sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====tag is %ld ",(long)[sender tag]);
    Merchant *aMerchant = [merchantList objectAtIndex:[sender tag]];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    // NSLog(@"addressUrl  is ....%@",addressUrl);
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}
#pragma mark- appendString Method

-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}



-(NSString *)deviceUDID
{
    
    NSString *uid;
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
    {
        uid= [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
    }
    else
    {
        uid = [self advertisingIdentifier];
        if ([uid isKindOfClass:NULL]) {
            uid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        }
        
    }
    
    
    //NSLog(@"UDID is %@ ",uid);
    
    return uid;
}
- (NSString *) advertisingIdentifier
{
    if (!NSClassFromString(@"ASIdentifierManager")) {
        SEL selector = NSSelectorFromString(@"uniqueIdentifier");
        if ([[UIDevice currentDevice] respondsToSelector:selector]) {
            return [[UIDevice currentDevice] performSelector:selector];
        }
        
    }
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}
/*
 NSLog(@">>>>-----------------------------------------------");
 NSLog(@"stamps height:%f",stampHeight);
 NSLog(@">>>>------%f",stampWidth*firstRowStampsCount+(firstRowStampsCount -1)*DEFAULTSPACE);
 NSLog(@"stampsView.frame.size.width:;%f",stampsView.frame.size.width);
 NSLog(@"stampIconContainer.frame.size.width:;%f",stampIconContainer.frame.size.width);
 NSLog(@"stampIconContainer.frame.x:;%f",stampIconContainer.frame.origin.x);
 NSLog(@">>>>---------------------------------------------");
 */

@end
