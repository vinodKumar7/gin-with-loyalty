//
//  KeyboardViewController.m
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 17/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.




#import <UIKit/UIKit.h>
#import "KeyboardViewController.h"
#import "ZenKeyboard.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"



@interface KeyboardViewController ()

{
    
    ASIFormDataRequest *redeemRequest;
    
    AppDelegate *appDelegate;
    ZenKeyboard *keyboard;
    UIView *loadingView;
}
@end

@implementation KeyboardViewController
@synthesize presentTextField, currentTextField, accessoryView,merchantMustLabel, product_ID, product_imageView, lblCoffeeFree, lblProductName,productName,productOffer,productImage,redeemPassword,product_Image, deviceID,multipleRedeemCount,isLastStamp,isFromFreebies,product_Name;



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChangeOneCI:)
                                                           name:UITextFieldTextDidChangeNotification
                                                           object:presentTextField];
    
    self.view.userInteractionEnabled = YES;
    merchantMustLabel.backgroundColor=[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willShowKeyboard:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didShowKeyboard:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    

}

- (void)willShowKeyboard:(NSNotification *)notification {
    [UIView setAnimationsEnabled:NO];
}

- (void)didShowKeyboard:(NSNotification *)notification {
    [UIView setAnimationsEnabled:NO];
}
- (void) viewWillAppear:(BOOL)animated
{
   // product_imageView.image             = product_Image;
    product_imageView.layer.borderWidth = 2.0;
    product_imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    keyboard = [[ZenKeyboard alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/1.7, self.view.bounds.size.width, 276)];
    
    keyboard.textField    = presentTextField;
    presentTextField.text = @"";
    [presentTextField becomeFirstResponder];
    NSLog(@"product_ID is %@ product_Image is %@ product_Name is %@",product_ID,product_Image,product_Name);
   
    
    if(self.isLastStamp)
        NSLog(@" LAST STAMP");
    else
        NSLog(@"NOT LAST STAMP");
}
-(BOOL)prefersStatusBarHidden{
    
    return YES;
}

#pragma mark- User Defined Methods

-(void)validatingPIN
{
    loadingView=[[UIView alloc]initWithFrame:self.view.bounds];
    loadingView.backgroundColor=[UIColor blackColor];
    UIActivityIndicatorView *activityIndicator;
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = CGRectMake(0.0, 0.0, 100.0, 100.0);
    activityIndicator.center = self.view.center;
    [activityIndicator startAnimating];
    presentTextField.text=@"";
    [loadingView addSubview: activityIndicator];
    [self.view addSubview:loadingView];
    
}



-(void)changeValuesOnKeyboard{}

-(void)textFieldTextDidChangeOneCI:(NSNotification *)notification
{
    if (isFromFreebies) {
        
    }
    else{
   
    if ([presentTextField.text length]== 4) {
        NSLog(@"redeemPassword is %@ ",redeemPassword);
        if ([presentTextField.text isEqualToString:redeemPassword]) {
            presentTextField.text=@"";
            presentTextField.tag=100;
            NSLog(@"coungt is %@ ",multipleRedeemCount);
        //http://www.nabtest.net/newapp/redeemed.php
            //http://184.107.152.53/newapp/redeemed.php
            NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
            NSURL *url = [NSURL URLWithString:@"http://184.107.152.53/newapp/redeemed.php"];

            redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [redeemRequest setPostValue:deviceID forKey:@"user_id"];
            [redeemRequest setPostValue:product_ID forKey:@"pid"];
            [redeemRequest setPostValue:@"24" forKey:@"cid"];
            [redeemRequest setPostValue:@"0.0"forKey:@"lat"];
            [redeemRequest setPostValue:@"0.0" forKey:@"lon"];
            [redeemRequest setPostValue:multipleRedeemCount forKey:@"count"];
            
            [redeemRequest setDelegate:self];
            [redeemRequest startAsynchronous];
        }
        else
        {
            presentTextField.text=@"";
        presentTextField.tag=1001;
        }
        
    }
    }
}

#pragma mark- UIButton Action Methods

- (IBAction)backBtnPressed:(id)sender {
    
//    [presentTextField resignFirstResponder];
    presentTextField.text=@"";
    
//    [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
    
}

#pragma mark- UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.currentTextField = textField;
    [textField setInputAccessoryView:accessoryView];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField.tag==100) {
        return (newLength > 4) ? NO : YES;
    }
    else
        return (newLength > 3) ? NO : YES;
}



#pragma mark- Request Finished and Request Failed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"redeemRequest response 98 %@ 98",[request responseString]);
    
    NSString *str = [NSString stringWithString:[request responseString]];
    //int len = (int)[str length] - 1;
   
    if (str)
    {
    
    
    
    
    if([str isEqualToString:@"success"] && isLastStamp){
        NSLog(@"appDelegate.isFromFreebies = YES in keyboardView");
            appDelegate.isFromFreebies = YES;
    }
    }
//        [presentTextField resignFirstResponder];
//        [self dismissViewControllerAnimated:NO completion:nil];
    
    [self backBtnPressed:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:@"Cancel",nil];
    [alert_view show];
    
   
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    
    isLastStamp = NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
