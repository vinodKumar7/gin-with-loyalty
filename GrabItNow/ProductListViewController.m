//
//  ProductListViewController.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductCell.h"
#import "Product.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "NSString_stripHtml.h"
#import <QuartzCore/QuartzCore.h>

#import "ProductDetailViewController.h"
#define ProductCellHeight 84.0;

@interface ProductListViewController ()
{
    NSMutableDictionary * imageDownloadsInProgress;
    int currPageNo;
    int pageSize;
    NSString *searchCategoryId;
    NSString *searchKeyword;
    NSString *searchLocation;
    NSString *redeemProductId;
    NSString *redeemProName;
    AppDelegate *appDelegate;
    ASIFormDataRequest * categorySearchRequest;
    ASIFormDataRequest * productFetchRequest;
    ASIFormDataRequest *redeemRequest;
    ProductListParser *productsXMLParser;
    ProductDataParser *productDataXMLParser;
    BOOL allProductsLoaded;
    NSIndexPath * openProductIndex;
    
    ProductViewController *productController;
    
    NSString *serviceName;
    
}

@property (nonatomic, strong) NSIndexPath * openProductIndex;
@property (nonatomic, strong) NSMutableDictionary * imageDownloadsInProgress;
@property (nonatomic, strong) ProductViewController *productController;
- (void)startIconDownload:(Product *)aProduct forIndexPath:(NSIndexPath *)indexPath;
- (void) searchProductsOnKeywordBasis;
- (void) loadMoreProducts;
@end

@implementation ProductListViewController


@synthesize noProductsFoundLabel;
@synthesize productsList;
@synthesize imageDownloadsInProgress;
@synthesize openProductIndex;
@synthesize productController;
@synthesize tblView;
@synthesize productListType;
@synthesize redeemActivityIndicator;
@synthesize locationManager;
@synthesize userLocation;
// Coupon related parameters
@synthesize couponView;
@synthesize couponMerchantImage;
@synthesize couponImageView;
@synthesize TandCLabel;
@synthesize TandCButton;
@synthesize myImage;
@synthesize couponBgImageView;
@synthesize couponCardBg_ImageView;
@synthesize couponCardBackBg_ImageView;

@synthesize TandCView;
@synthesize couponFrontView;
@synthesize couponBackView;
@synthesize couponTandCTextView;
@synthesize TandCBackButton;
@synthesize couponFlipView;
@synthesize redeemButton;
@synthesize frontRedeemButton;
@synthesize discountLabel;
@synthesize memNameLabel,memNumberLabel,clientLabel;
@synthesize productNameLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productListType:(ProductListType) productListTypeLocal
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        imageDownloadsInProgress = [[NSMutableDictionary alloc] init];
        productsList = [[NSMutableArray alloc] init];
        allProductsLoaded = NO;
        pageSize = 10;
        openProductIndex = nil;
        self.productListType = productListTypeLocal;
        noProductsFoundLabel.hidden = YES;
    }
    return self;
}

- (void)dealloc {
    if (imageDownloadsInProgress) {
        NSArray *iconDownloaders = [imageDownloadsInProgress allValues];
        [iconDownloaders makeObjectsPerformSelector:@selector(cancelDownload)];
        [imageDownloadsInProgress removeAllObjects];
        imageDownloadsInProgress = nil;
    }
}

- (void) updateHeaderDetails {
    
    [appDelegate.homeViewController hideBackButon:YES];
    
    if (self.productListType == ProductListTypeSearch) {
        [appDelegate.homeViewController setHeaderTitle:@"Search Products"];
        [appDelegate.homeViewController hideBackButon:NO];
        
       
    }
    else if (self.productListType == ProductListTypeMyFavorites) {
        [appDelegate.homeViewController setHeaderTitle:@"My Favourites"];
    }
    else if (self.productListType == ProductListTypeNearbyMe) {
        [appDelegate.homeViewController setHeaderTitle:@"What's Around Me"];
    }
    else if (self.productListType == ProductListTypeOffers) {
        [appDelegate.homeViewController setHeaderTitle:@"Hot Offers"];
    }
    else if (self.productListType == ProductListTypeLoyalty) {
        [appDelegate.homeViewController setHeaderTitle:@"Loyalty"];
    }
    
}

- (void) configureController {
    
    switch (productListType) {
        case ProductListTypeSearch:
            serviceName = @"search.php";
            
            break;
        case ProductListTypeOffers:
            serviceName = @"get_hot_offer.php";
            
            break;
            
        default:
            break;
    }
    
}

- (void)fetchMyFavoritesAndUpdateList {
    
    if (productsList.count > 0) {
        [productsList removeAllObjects];
    }
    
    [productsList addObjectsFromArray:[appDelegate myFavoriteProducts]];
    
    // If No products, hide table
    if (productsList.count == 0) {
        [self noProductsFound:YES];
    }
    else {
        [self noProductsFound:NO];
    }
    
    [self.tblView reloadData];
}

- (void) noProductsFound:(BOOL) notFound {
    
    if (notFound) {
        self.tblView.hidden = YES;
        self.noProductsFoundLabel.text = @"No Favourites Added";
        self.noProductsFoundLabel.hidden = NO;
        self.noProductsFoundLabel.backgroundColor=[UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
        self.noProductsFoundLabel.layer.cornerRadius = 7.5;
        self.noProductsFoundLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        self.noProductsFoundLabel.layer.borderWidth = 2.0;
    }
    else {
        self.tblView.hidden = NO;
        self.noProductsFoundLabel.hidden = YES;
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //userlocation tracking
    


    
    [self configureController];
    couponMerchantImage.layer.cornerRadius = 5.0;
    couponMerchantImage.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    couponMerchantImage.layer.borderWidth = 2.0;

    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    if (self.productListType == ProductListTypeMyFavorites) {
        [self fetchMyFavoritesAndUpdateList];
    }
    
    // Load Initial products if showing Hot-Offers
    if((productListType == ProductListTypeOffers) && (productsList.count == 0)) {
        [self searchProductsOnKeywordBasis];
    }
    
    [self updateHeaderDetails];
    
    [tblView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"Prodcuts list frame = %@",NSStringFromCGRect(self.view.frame));
}


-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
   // locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}

- (void) setInitialProductsList:(NSArray *) plist {
    
    if (!productsList) {
        productsList = [[NSMutableArray alloc] initWithArray:plist];
    }
        else {
        [productsList removeAllObjects];
        [productsList addObjectsFromArray:plist];
    }
    
    if([plist count]< 30){
        NSLog(@"Products count less than table size");
        allProductsLoaded = YES;
    }
    
    
    [tblView reloadData];
}

- (void) setSearchCategoryID:(NSString *) catId keyword:(NSString *) key location:(NSString *) location {
    searchCategoryId = catId;
    searchKeyword = key;
    searchLocation = location;
}

#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (categorySearchRequest && (self.productListType != ProductListTypeMyFavorites)) {
        return (productsList.count + 1);
       
    }
    
    return productsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // NSLog(@"tableView cellForRowAtIndexPath");
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == productsList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        loadingCell.textLabel.text = @"Loading ....";
        loadingCell.textLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:255/255.0 green:97/255.0 blue:55/255.0 alpha:1.0];
        
        loadingCell.backgroundColor = [UIColor whiteColor];
        
        UIActivityIndicatorView *loadingActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loadingActivityView.backgroundColor = [UIColor clearColor];
        [loadingActivityView startAnimating];
        loadingCell.accessoryView = loadingActivityView;
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    
    Product *aPro = [productsList objectAtIndex:indexPath.row];
    
    
    // Set title and offer text here
    cell.productNameLabel.text = aPro.productName;
    cell.productOfferLabel.text = aPro.productOffer;
//    cell.productImg.hidden = NO;
    cell.productImg.hidden = YES;
    
    if ([appDelegate productExistsInFavorites:aPro])
        cell.msgImage.image = [UIImage imageNamed:@"heartBlue.png"];
    else
        cell.msgImage.image = [UIImage imageNamed:@"heartWhite.png"];
    
    cell.imgLoadingIndicator.hidesWhenStopped = YES;
    cell.accesoryImageView.hidden = NO;
    cell.accesoryImageView.image = [UIImage imageNamed:@"arrow.png"];
    
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0) {
        //        cell.productNameLabel.textColor=[UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        
    }
    else if (rowModuleNo == 1) {
        //        cell.productNameLabel.textColor=[UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    }

    cell.headerContainerView.backgroundColor = [UIColor clearColor];
    

    
    // If valid, make a loadMoreProducts call
    if ([self isLoadMoreValid] && (indexPath.row == (productsList.count - 1)))
    {
        // since last cell reached, load more products
        [self loadMoreProducts];
    }
    
    

    
    // If favorites list, show remove favorite button.
    if (self.productListType == ProductListTypeMyFavorites) {

        cell.msgImage.image = [UIImage imageNamed:@"heartBlue.png"];
        cell.removeFavoritebutton.tag = indexPath.row;
        cell.removeFavoritebutton.hidden = NO;
        cell.productImg.hidden = YES;
        cell.accesoryImageView.hidden = YES;
        [cell.removeFavoritebutton addTarget:self action:@selector(removeFavoriteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
        
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void) removeFavoriteAction:(id) sender {
    
    int index = [sender tag];
    Product *pro = [productsList objectAtIndex:index];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",pro.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    alert.tag = 22 + index;
   
    [alert show];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"tableView:heightForRowAtIndexPath:");
    
//    if (openProductIndex && (indexPath.row == openProductIndex.row) ) {
//        NSLog(@"ROW=%d toBeHeight=%f",indexPath.row,self.tblView.frame.size.height);
//        return self.tblView.frame.size.height;
//    }
//    
//    if (indexPath.row == productsList.count) {
//        return 44.0;
//    }
    
    return ProductCellHeight;
}

/** Delegate methods **/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    

    Product *pro = [productsList objectAtIndex:indexPath.row];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];

    ProductDetailViewController *productDVC = [storyboard instantiateViewControllerWithIdentifier:@"productDetailViewController"];
    productDVC.product_ID = pro.productId;
    [self.navigationController pushViewController:productDVC animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isLoadMoreValid {
    if (!allProductsLoaded
        && (productListType != ProductListTypeMyFavorites)
        && !openProductIndex
        && !categorySearchRequest
        ) {
        
        return YES;
    }
    
    // In all other cases cannot load more
    return NO;
}


- (void) loadMoreProducts {
    NSLog(@"loadMoreProducts");
    if ([self isLoadMoreValid]) {
        [self searchProductsOnKeywordBasis];
    }
}


- (void) fetchProductWithProductID:(NSString *) productId {
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    
    NSLog(@"Product Detail URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    
}


- (void) searchProductsOnKeywordBasis {
    
    //[self showActivityView];
    
    if (categorySearchRequest) {
        // A request is already in progress, return
        return;
    }
    
    NSLog(@"fetching more products");
    
    // Bring the table to the bottom
    if (tblView.contentSize.height > tblView.frame.size.height) {
        [tblView setContentOffset:CGPointMake(tblView.contentOffset.x, (tblView.contentSize.height - tblView.frame.size.height + 44.0) ) animated:YES];
    }
    
    int startPageNumber = [productsList count];
    NSString *urlString;
    
    if(self.productListType == ProductListTypeSearch)
    {
//        urlString = [NSString stringWithFormat:@"%@%@?cat_id=%@&p=%@&q=%@&cid=%@&country=%@&start=%d&limit=%d",URL_Prefix,serviceName,searchCategoryId,searchLocation,searchKeyword,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country,startPageNumber,pageSize];
//        
//
        
        if ([searchKeyword isEqualToString:@"" ]||[searchLocation isEqualToString:@""])
        
            urlString = [NSString stringWithFormat:@"%@%@?cat_id=%@&p=&q=&cid=%@&country=%@&start=%d&limit=%d",URL_Prefix,serviceName,searchCategoryId,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country,startPageNumber,pageSize];
            
        
        else
        
            urlString = [NSString stringWithFormat:@"%@%@?cat_id=%@&p=%@&q=%@&cid=%@&country=%@&start=%d&limit=%d",URL_Prefix,serviceName,searchCategoryId,searchLocation,searchKeyword,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country,startPageNumber,pageSize];
        
 NSLog(@"Search URL in Products List: %@",urlString);
    }
    else if(self.productListType == ProductListTypeOffers)
    {
        urlString = [NSString stringWithFormat:@"%@%@?cid=%@",URL_Prefix,serviceName,appDelegate.sessionUser.client_id];
        
        NSLog(@"Hot Offers... URL: %@",urlString);
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    categorySearchRequest = [ASIFormDataRequest requestWithURL:url];
    [categorySearchRequest setDelegate:self];
    [categorySearchRequest startAsynchronous];
    [self.tblView reloadData];
    
}

#pragma mark -- AlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        // Remove from fav case
        if(alertView.tag!=222)
        {
           // AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            int index = alertView.tag - 22;
            Product *removePro = [productsList objectAtIndex:index];
            BOOL success = [appDelegate removeProductFromfavorites:removePro];
            
            //[self updateFavoriteStatus];
            
            if (success) {
                [self productremovedFromFavorites:removePro];}
        }
    }
    if(alertView.tag == 222)
    {
        NSLog(@"Redeem alert view and button index::%d",buttonIndex);
        if(buttonIndex == 1)
        {
            //[self getCurrentLocation];
            NSString *lattitude = @"0.00"; // [NSString stringWithFormat:@"%f",self.userLocation.latitude];
            NSString *longitude =@"0.00"; //[NSString stringWithFormat:@"%f",self.userLocation.longitude];
            NSLog(@"%@",lattitude);
            NSLog(@"%@",longitude);
            
            NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
            NSURL *url = [NSURL URLWithString:urlString];
            NSLog(@"url is:==> %@",url);
            
            
            redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
            [redeemRequest setPostValue:redeemProductId forKey:@"pid"];
            [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
            [redeemRequest setPostValue:lattitude forKey:@"lat"];
            [redeemRequest setPostValue:longitude forKey:@"lon"];
            
            [redeemRequest setDelegate:self];
            [redeemRequest startAsynchronous];
            [self.redeemActivityIndicator startAnimating];
            
            
        }
        
    }
    if(alertView.tag == 333)
    {
        NSLog(@"ThankYou alert view and button index::%d",buttonIndex);
        if(buttonIndex == 0)
        {
            
            [self handleCouponTap:nil];
            
        }
    }
}

/*
#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(Product *)aProduct forIndexPath:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader == nil)
    {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.aProduct = aProduct;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        [imageDownloadsInProgress setObject:iconDownloader forKey:indexPath];
        [iconDownloader startDownload];
    }
}

// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    
    //NSLog(@"appImageDidLoad: for row %d",indexPath.row);
    
    if (iconDownloader != nil)
    {
        //NSLog(@"-- Icon downloader exists");
        
        ProductCell *cell = (ProductCell *)[self.tblView cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        if (! iconDownloader.aProduct.imageDoesntExist) {
            //NSLog(@"-- Image exists");
            // Display the newly loaded image
            cell.productImg.image = iconDownloader.aProduct.productImage;
        }
        else {
            //NSLog(@"-- Image doesn't exist");
            // Display Dummy image
            cell.productImg.hidden = YES;
            cell.noImageLabel.hidden = NO;
        }
        
        
        // Stop activity animation
        [cell.imgLoadingIndicator stopAnimating];
    }
    else {
        //NSLog(@"-- Icon downloader doesn't exists");
    }
    
    // Remove the IconDownloader from the in progress list.
    // This will result in it being deallocated.
    [imageDownloadsInProgress removeObjectForKey:indexPath];
}
*/

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSLog(@"ProdcutsListVC -- requestFinished:");
    
    if (request == categorySearchRequest) {
        
        categorySearchRequest = nil;
        
        NSLog(@"Product List RES: %@",[request responseString]);
        
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        
    }
    if (request == productFetchRequest) {
        productFetchRequest = nil;
        NSLog(@"Product ** RES: %@",[request responseString]);
        
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        NSString *msg = [NSString stringWithFormat:@"For redeeming this offer %@",redeemProName];
        if([[request responseString] isEqualToString:@"success"])
        {
            
            UIAlertView *thanksAlert = [[UIAlertView alloc]initWithTitle:@"Thank You!" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
            thanksAlert.tag = 333;
            [thanksAlert show];
            
        }

    }
    

}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    NSLog(@"ProdcutsListVC -- requestFailed:");
    
    if (request == categorySearchRequest) {
        categorySearchRequest = nil;
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];

        [self.tblView reloadData];
    }
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];

        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
    }
    else {
        productFetchRequest = nil;
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];

    }
    

}

#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal {
    
    // Here update products list
    if (!productsList) {
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    }
    else {
        [productsList addObjectsFromArray:prodcutsListLocal];
    }
    
    
    
    // ** Update counter if there are no more products to load
    if ([prodcutsListLocal count] < pageSize || (self.productListType == ProductListTypeOffers)) {
        NSLog(@"checking all products or not.....");
        allProductsLoaded = YES;
    }
    
    
    
    // Hide table if products list empty
    if (productsList.count == 0) {
        [self noProductsFound:YES];
    }
    else {
        [self noProductsFound:NO];
    }
    
    [self.tblView reloadData];
}

- (void)parsingProductListXMLFailed {
    
}

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product {
    
    NSLog(@"parsingProductDataFinished");
    [productsList replaceObjectAtIndex:openProductIndex.row withObject:product];
    
    if (productController) {
        
        [productController updateUIWithProductDetails:product];
        
    }
    
    
}

- (void)parsingProductDataXMLFailed {
    
}

#pragma mark -- Product view delegate methods

- (void)productAddedToFavorites:(Product *)pro {
    
    if (self.productListType == ProductListTypeMyFavorites) {
        
        openProductIndex = nil;
        [self.tblView reloadData];
    }

}

- (void)productremovedFromFavorites:(Product *)pro {
    if (self.productListType == ProductListTypeMyFavorites) {
        [self.productsList removeObject:pro];
        openProductIndex = nil;
        [self.tblView reloadData];
    }
    
    if (productsList.count == 0) {
        [self noProductsFound:YES];
    }
    else {
        [self noProductsFound:NO];
    }
    
}

- (void) showProductCouponForProduct:(Product *) pro couponImage:image

{   if ([pro.mobilecoupon isEqualToString:@"0"])
{
    UIAlertView *CouponAlert=[[UIAlertView alloc]initWithTitle:@"This is NOT a coupon offer" message:@"  Refer to offer terms for redemption instructions" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    CouponAlert.tag=123;
    [CouponAlert show];
    
}
else

{
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponGestureSelector:)];
    //recognizer.delegate = self;
    
    self.myImage = image;
    [couponView addGestureRecognizer:recognizer];
    couponView.frame = self.view.bounds;
    
    // Assign text and image
    redeemProductId = pro.productId;
    discountLabel.text = [NSString stringWithFormat:@"%@",pro.productOffer];
    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    clientLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
    memNumberLabel.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
    couponMerchantImage.image =self.myImage;// image;//pro.productImage;
    productNameLabel.text = pro.productName;
    redeemProName = pro.productName;
    
    //===========
    NSLog(@"T&C: %@",pro.productTermsAndConditions);
    NSString *htmlString = pro.productTermsAndConditions;
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&"
                                                       withString:@"and"];
    
    couponTandCTextView.text = [htmlString stripHtml];

    
    couponView.hidden = NO;
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
    [self.view addSubview:couponView];
    
    // Finalizing statements
    couponCardBg_ImageView.frame = couponFlipView.bounds;
    couponCardBackBg_ImageView.frame =  couponFlipView.bounds;
    couponBackView.frame = couponFlipView.bounds;
    couponFrontView.frame = couponFlipView.bounds;
    
    //======== configuring couponcard buttons and labels
    
    //buttons
    CGRect TandCButtonFrame = TandCButton.frame;
    TandCButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - TandCButtonFrame.size.width-22;
    if([appDelegate isIphone5])
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    TandCButton.frame = TandCButtonFrame;
    
    
    CGRect fRedeemButtonFrame = frontRedeemButton.frame;
    fRedeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - fRedeemButtonFrame.size.width-194;

    if([appDelegate isIphone5])
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    frontRedeemButton.frame = fRedeemButtonFrame;
    
    
    
    CGRect TandCBackButtonFrame = TandCBackButton.frame;
    TandCBackButtonFrame.origin.x = 40.0;
    TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-65;
    TandCBackButton.frame = TandCBackButtonFrame;
    
    CGRect redeemButtonFrame = redeemButton.frame;
    redeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - redeemButtonFrame.size.width - 23.0;
    redeemButtonFrame.origin.y = couponBackView.frame.size.height-65;
    redeemButton.frame = redeemButtonFrame;
    
    //labels
    
    CGRect discountLabelFrame = discountLabel.frame;
    discountLabelFrame.origin.y = self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height+10;
    discountLabel.frame = discountLabelFrame;
    
    
    
    CGRect nameLabelFrame = memNameLabel.frame;
    
    if([appDelegate isIphone5])
       nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height;
    else 
       nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height;
    memNameLabel.frame = nameLabelFrame;
    
    CGRect memnumLabelFrame = memNumberLabel.frame;
    memnumLabelFrame.origin.y = self.memNameLabel.frame.origin.y + self.memNameLabel.frame.size.height;
    memNumberLabel.frame = memnumLabelFrame;
    
    CGRect clientLabelFrame = clientLabel.frame;
    clientLabelFrame.origin.y = self.memNumberLabel.frame.origin.y + self.memNumberLabel.frame.size.height;
    clientLabel.frame = clientLabelFrame;
    
    CGRect couponBgFrame = couponBgImageView.frame;
    if([appDelegate isIphone5])
    couponBgFrame.size.height = 170;
    else  couponBgFrame.size.height = 120;
    couponBgImageView.frame = couponBgFrame;
    
        //===========
    
    
    
    [couponFlipView addSubview:couponBackView];
    [couponFlipView addSubview:couponFrontView];
    couponBackView.alpha = 0.0;
    couponFrontView.alpha = 1.0;
}
}

- (void) couponGestureSelector:(UIGestureRecognizer *) gesRec {
    
    CGPoint touchPoint = [gesRec locationOfTouch:0 inView:couponView];
    
    if (couponFrontView.alpha == 0.0) {
        // i.e Front view is not visible, but backView is
        if( CGRectContainsPoint(TandCBackButton.frame, touchPoint) ) {
            [self termsAndCondBackButtonPressed:TandCBackButton];
        }
        else if( CGRectContainsPoint(redeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
    else {
        // i.e Back view is not visible, but frontView is
        if( CGRectContainsPoint(TandCButton.frame, touchPoint) ) {
            [self termsAndCondButtonPressed:TandCButton];
        }
        else if( CGRectContainsPoint(frontRedeemButton.frame, touchPoint) )
        {
            [self redeemButtonPressed:redeemButton];
        }

        else {
            [self handleCouponTap:nil];
        }
    }
    
    
    
}

-(IBAction)handleCouponTap:(id)sender
{
    NSLog(@"handleCouponTap:");
    //couponView.hidden = YES;
    
    // Remove recognizer
    for (UIGestureRecognizer *gesRec in couponView.gestureRecognizers) {
        [couponView removeGestureRecognizer:gesRec];
    }
    
    [couponView removeFromSuperview];
}

/*
- (void)showMailComposer:(NSArray *)recepients withBody:(NSString *)text {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"Subject Goes Here."];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        [self presentModalViewController:mailViewController animated:YES];
        
    }
    
    else {
        
        NSLog(@"\nDevice is unable to send email in its current state.");
        
    }

    
    }

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
 
}

*/

- (IBAction) termsAndCondButtonPressed:(id)sender {
    NSLog(@"PLVC termsAndCondButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 0.0;
    couponBackView.alpha = 1.0;
    [UIView commitAnimations];
    
}

- (IBAction) termsAndCondBackButtonPressed:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 1.0;
    couponBackView.alpha = 0.0;
    [UIView commitAnimations];
}
- (IBAction) redeemButtonPressed:(id) sender { 
    //[delegate showMailComposer:nil withBody:nil];
    //[appDelegate.homeViewController showMailComposer:nil withBody:nil];
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use, so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
    [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    
    redemAlert.tag = 222;
    [redemAlert show];
}

    
#pragma mark - CLLocationManagerDelegate
    
    - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
    {
        NSLog(@"didFailWithError: %@", error);
        
    }
    
    - (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
    {
        // NSLog(@"didUpdateToLocation: %@", newLocation);
        self.userLocation = [newLocation coordinate];
        // NSLog(@"lattitude::%f",self.userLocation.latitude);
        // NSLog(@"longitude::%f",self.userLocation.longitude);
        
        
        
    }
   
    
    
@end
